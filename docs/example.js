var pageHeader = <AUI.PageHeader headerText="AUI (React) Documentation" />;

var nav = (
    <AUI.NavGroup>
        <AUI.NavHeading>Components</AUI.NavHeading>
        <AUI.Nav>
            <AUI.NavItem href="#avatars">Avatars</AUI.NavItem>
            <AUI.NavItem href="#badges">Badges</AUI.NavItem>
            <AUI.NavItem href="#banners">Banners</AUI.NavItem>
            <AUI.NavItem href="#buttons">Buttons</AUI.NavItem>
            <AUI.NavItem href="#button-group">Button group</AUI.NavItem>
            <AUI.NavItem href="#icons">Icons</AUI.NavItem>
            <AUI.NavItem href="#lozenges">Lozenges</AUI.NavItem>
            <AUI.NavItem href="#tables">Tables</AUI.NavItem>
        </AUI.Nav>
        <AUI.NavHeading>Page layouts</AUI.NavHeading>
        <AUI.Nav>
            <AUI.NavItem href="#application-header">Application header</AUI.NavItem>
        </AUI.Nav>
    </AUI.NavGroup>
);

var globalIcons = ["add","add-comment","add-small","approve","appswitcher","arrows-down","arrows-left","arrows-right","arrows-up","attachment","attachment-small","autocomplete-date","back-page","blogroll","bp-decisions","bp-default","bp-files","bp-requirements","bp-howto","bp-jira","bp-meeting","bp-retrospective","bp-sharedlinks","bp-troubleshooting","build","calendar","close-dialog","collapsed","comment","configure","confluence","copy-clipboard","custom-bullet","delete","deploy","details","doc","down","drag-vertical","edit","edit-small","email","error","expanded","file-code","file-doc","file-java","file-pdf","file-ppt","file-txt","file-wav","file-xls","file-zip","flag","focus","group","handle-horizontal","help","hipchat","homepage","image","image-extrasmall","image-small","info","like","like-small","weblink","link","list-add","list-remove","locked","locked-small","macro-code","macro-default","macro-gallery","macro-status","more","nav-children","page-blank","page-blogpost","page-default","page-template","pages","quote","redo","remove","remove-label","review","rss","search","search-small","share","sidebar-link","sourcetree","space-default","space-personal","star","success","table-bg","table-col-left","table-col-remove","table-col-right","table-copy-row","table-cut-row","table-header-column","table-header-row","table-merge","table-no-bg","table-paste-row","table-remove","table-row-down","table-row-remove","table-row-up","table-split","teamcals","time","undo","unfocus","unlocked","unstar","unwatch","up","user","user-status","view","view-card","view-list","view-table","warning","watch","workbox","workbox-empty","configure-columns","export","export-list","file-image","admin-fusion","admin-jira-fields","admin-issue","admin-notifications","admin-roles","admin-jira-screens","pause","priority-highest","priority-high","priority-medium","priority-low","priority-lowest","refresh-small","share-list","switch-small","version","workflow","admin-jira-settings","component","reopen","roadmap","deploy-success","deploy-fail","file-generic","arrow-down","arrow-up","file-video","blogroll-large","email-large","layout-1col-large","layout-2col-large","layout-2col-left-large","layout-2col-right-large","layout-3col-center-large","layout-3col-large","nav-children-large","pages-large","sidebar-link-large","teamcals-large","user-large","jira-issues"];
var devToolsIcons = ["devtools-arrow-left","devtools-arrow-right","devtools-branch","devtools-branch-small","devtools-browse-up","devtools-checkout","devtools-clone","devtools-commit","devtools-compare","devtools-file","devtools-file-binary","devtools-file-commented","devtools-folder-closed","devtools-folder-open","devtools-fork","devtools-pull-request","devtools-repository","devtools-repository-forked","devtools-repository-locked","devtools-side-diff","devtools-submodule","devtools-tag","devtools-tag-small","devtools-task-cancelled","devtools-task-disabled","devtools-task-in-progress","bitbucket"];
var editorIcons = ["editor-align-center","editor-align-left","editor-align-right","editor-bold","editor-color","editor-emoticon","editor-help","editor-hr","editor-indent","editor-italic","editor-layout","editor-list-bullet","editor-list-number","editor-macro-toc","editor-mention","editor-outdent","editor-styles","editor-symbol","editor-table","editor-task","editor-underline"];
var jiraIcons = ["jira","jira-completed-task","jira-test-session"];

ReactDOM.render(
    <div id="page">
        <AUI.ApplicationHeader logo="aui" />
        <AUI.PageContent
            pageHeader={pageHeader}
            nav={nav}>
            <h2 id="avatars">Avatars</h2>
            <div className="aui-flatpack-example">
                <AUI.Avatar size="xsmall" src="//docs.atlassian.com/aui/latest/docs/images/avatar-16.png" />
                <AUI.Avatar size="small" src="//docs.atlassian.com/aui/latest/docs/images/avatar-24.png" />
                <AUI.Avatar size="medium" src="//docs.atlassian.com/aui/latest/docs/images/avatar-32.png" />
                <AUI.Avatar size="large" src="//docs.atlassian.com/aui/latest/docs/images/avatar-48.png" />
                <AUI.Avatar size="xxlarge" src="//docs.atlassian.com/aui/latest/docs/images/avatar-96.png" />
                <AUI.Avatar size="small" src="//docs.atlassian.com/aui/latest/docs/images/project-24.png" isProject />
                <AUI.Avatar size="medium" src="//docs.atlassian.com/aui/latest/docs/images/project-32.png" isProject />
                <AUI.Avatar size="large" src="//docs.atlassian.com/aui/latest/docs/images/project-48.png" isProject />
                <AUI.Avatar size="xlarge" src="//docs.atlassian.com/aui/latest/docs/images/project-64.png" isProject />
                <AUI.Avatar size="xxxlarge" src="//docs.atlassian.com/aui/latest/docs/images/project-128.png" isProject />
            </div>
            <h2 id="badges">Badges</h2>
            <div className="aui-flatpack-example">
                <AUI.Badge text="1" />
                <AUI.Badge text="2" />
                <AUI.Badge text="3" />
                <AUI.Badge text="4" />
                <AUI.Badge text="5" />
                <AUI.Badge text="6" />
                <AUI.Badge text="7" />
            </div>
            <h2 id="banners">Banners</h2>
            <div className="aui-flatpack-example" style={{height:80}}>
                <AUI.Banner>Your <strong>license has expired!</strong> There are two days left to <a>renew your license</a>.</AUI.Banner>
                <AUI.ApplicationHeader logo="aui" />
            </div>
            <h2 id="buttons">Buttons</h2>
            <div className="aui-flatpack-example">
                <AUI.Button>Button</AUI.Button>
                <AUI.Button type="primary">Primary Button</AUI.Button>
                <AUI.Button type="link">Link Button</AUI.Button>
                <AUI.Button icon="view">Icon Button</AUI.Button>
                <AUI.Button disabled>Disabled Button</AUI.Button>
                <AUI.Button type="subtle" icon="configure">Subtle Button</AUI.Button>
            </div>
            <h2 id="button-group">Button group</h2>
            <div className="aui-flatpack-example">
                <AUI.ButtonGroup>
                    <AUI.Button>Button 1</AUI.Button>
                    <AUI.Button type="primary">Button 2</AUI.Button>
                </AUI.ButtonGroup>
            </div>
            <h2 id="icons">Icons</h2>
            <div className="aui-flatpack-example" id="icon-lists">
                <h5>Global icons</h5>
                {globalIcons.map(function(icon) {
                    return <AUI.Icon icon={icon} key={icon} />
                })}
                <h5>Dev tools icons</h5>
                {devToolsIcons.map(function(icon) {
                    return <AUI.Icon icon={icon} key={icon} />
                })}
                <h5>Editor icons</h5>
                {editorIcons.map(function(icon) {
                    return <AUI.Icon icon={icon} key={icon} />
                })}
                <h5>JIRA icons</h5>
                {jiraIcons.map(function(icon) {
                    return <AUI.Icon icon={icon} key={icon} />
                })}
            </div>
            <h2 id="lozenges">Lozenges</h2>
            <div className="aui-flatpack-example">
                <p>
                    <AUI.Lozenge>Default</AUI.Lozenge>
                    <AUI.Lozenge type="success">Success</AUI.Lozenge>
                    <AUI.Lozenge type="error">Error</AUI.Lozenge>
                    <AUI.Lozenge type="current">Current</AUI.Lozenge>
                    <AUI.Lozenge type="new">New</AUI.Lozenge>
                    <AUI.Lozenge type="moved">Moved</AUI.Lozenge>
                </p>
                <p>
                    <AUI.Lozenge subtle>Default</AUI.Lozenge>
                    <AUI.Lozenge subtle type="success">Success</AUI.Lozenge>
                    <AUI.Lozenge subtle type="error">Error</AUI.Lozenge>
                    <AUI.Lozenge subtle type="current">Current</AUI.Lozenge>
                    <AUI.Lozenge subtle type="new">New</AUI.Lozenge>
                    <AUI.Lozenge subtle type="moved">Moved</AUI.Lozenge>
                </p>
            </div>
            <h2 id="tables">Tables</h2>
            <AUI.Table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Username</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Matt</td>
                        <td>Bond</td>
                        <td>mbond</td>
                    </tr>
                </tbody>
            </AUI.Table>
            <h2 id="application-header">Application header</h2>
            <div className="aui-flatpack-example" style={{height:40}}>
                <AUI.ApplicationHeader logo="aui" />
            </div>
        </AUI.PageContent>
    </div>
, document.getElementById('root'));
